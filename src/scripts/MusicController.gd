extends Node


var backgroundMusic = load("res://resources/sounds/Music_BG_Eco-Garden-Cheerful.wav")


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func play_music():
	$Music.stream = backgroundMusic
	$Music.play()
	
func stop_music():
	$Music.stream = backgroundMusic
	$Music.stop()
